#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 16:30:41 2020

@author: martien
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
import matplotlib.patheffects as pe
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pyproj import Proj, transform
import contextily as ctx
import matplotlib
from urllib.request import urlopen
from bs4 import BeautifulSoup
# from urllib.request import urlretrieve
import re
import ssl
import datetime
from datetime import date
from datetime import datetime
import glob
import seaborn as sns
from unicodedata import normalize

# from pathlib import Path
ssl._create_default_https_context = ssl._create_unverified_context
regex = re.compile('(.*\/(?P<name>\w+)\.(?P<ext>\w+))')


# from pysal.contrib.viz import mapping as maps
# https://github.com/darribas/contextily/blob/master/contextily_guide.ipynb
# https://stackoverflow.com/questions/56559520/change-background-map-for-contextily
# https://towardsdatascience.com/lets-make-a-map-using-geopandas-pandas-and-matplotlib-to-make-a-chloropleth-map-dddc31c1983d
# http://darribas.org/gds15/content/labs/lab_03.html
# https://gis.stackexchange.com/questions/78838/converting-projected-coordinates-to-lat-lon-using-python
# https://stackoverflow.com/questions/54088858/plotting-a-map-using-geopandas-and-matplotlib
# https://stackoverflow.com/questions/19073683/matplotlib-overlapping-annotations-text
# https://www.volksgezondheidenzorg.info/sites/default/files/map/detail_data/klik_corona06032020.csv
# https://stackoverflow.com/questions/51120802/extract-text-between-two-different-tags-beautiful-soup


try:
    os.chdir('/home/martien/Documents/QGIS/NLGemeenten/Gemeentegrenzen_2019/')
except:
    os.chdir('/home/martien/Documents/QGIS/NLGemeenten/Gemeentegrenzen_2019/')

print('Pandas:  %s.' % pd.__version__)
print('Numpy:   %s.' % np.__version__)
print('Geopandas:   %s.' % gpd.__version__)


def get_link(url):
    l = 0
    html = urlopen(url)
    soup = BeautifulSoup(html.read(), 'lxml')
    for link in soup.find_all('a', href=True):
        dlink = link.get('href')
        substr = re.search('.csv', dlink)
        if substr:
            #if regex.search(dlink).group('name').startswith('klik_'):
            print(dlink)
                # regex.search(dlink).group('name')
                # print(regex.search(dlink).group('ext'))
            l, naam = dlink, regex.search(dlink).group('name')
    return(l, naam)


def makemyday(paras):
    dag_tf, hour_tf = False, False
    for para in paras:
        print(para.getText())
        if 'updated' in para.getText():
            s = para.getText()
            m = re.search('(\d{2}\s+\w+\s+\d{4})', s)
            if not dag_tf:
                if m:
                    dag = m.group(1)
                    dag_tf = True
                    dag = normalize('NFKD', dag)
                else:
                    today = date.today()
                    dag = today.strftime('%d-%m-%Y')
            print(s)
            m = re.search('(\d+:\d{2}\s?\w{2})', s)
            if not hour_tf:
                if m:
                    hour = m.group(1)
                    print('Uur: ', hour)
                    hour = normalize('NFKD', hour)
                    hour= hour.replace(" ", "")
                    hour_tf = True
                else:
                    hour = "2:41pm"
    dag = datetime.strptime(dag + ' ' + hour, '%d %B %Y %I:%M%p')
    return(dag)


def read_nz_shape(fn, fcook):
    #inProj, outProj = Proj(init='epsg:4326'), Proj(init='epsg:4326')
    fn_prefix = '/home/martien/Documents/QGIS/statsnzdistrict-health-board-2015-SHP/'
    new_zealand = gpd.read_file(fn_prefix+fn, encoding = 'utf-8')
    layer  = gpd.read_file(fn_prefix+fcook, encoding = 'utf-8')
    layer.plot()
    new_zealand.plot()
    new_zealand = new_zealand.to_crs({'init': 'epsg:4326'})  # new_zealand = new_zealand.to_crs({'init': 'epsg:3395'}) #new_zealand = new_zealand.to_crs({'init': 'epsg:3035'})
    #new_zealand.plot()
    new_zealand = gpd.overlay(layer, new_zealand, how='intersection')
    new_zealand = new_zealand.to_crs({'init': 'epsg:3857'}) #  <-------------------------
    new_zealand.plot()
    new_zealand['DHB2015_Co'] = new_zealand['DHB2015_Co'].apply(pd.to_numeric, errors='coerce').fillna(0)
    new_zealand.rename(columns={'DHB2015_Na': 'DHB', 'DHB2015_Co': 'code'}, inplace=True)
    new_zealand.loc[new_zealand['DHB'].str.contains('Tairawhiti'), 'DHB']= "Tairāwhiti"
    new_zealand.set_index('DHB', inplace=True)
    new_zealand['centroid_column'] = new_zealand.geometry.centroid
    new_zealand["x"] = new_zealand.centroid.x
    new_zealand["y"] = new_zealand.centroid.y
    new_zealand.plot()
    return(new_zealand)

def make_plot(dfe, variable, fn, dpi, dag):
    dfe = dfe.to_crs({'init': 'epsg:3857'})
    #dfe, variable, fn, dpi = nzm, 'Total', fo, 75
    matplotlib.rcParams['text.usetex'] = True
    dfe[variable].fillna(0, inplace=True)
    dfe.dropna(subset=[variable], inplace=True)
    fig, ax = plt.subplots(1, figsize=(15, 15))
    plt.title(fn.replace("_", " "), fontsize=14)
    divider = make_axes_locatable(ax)
    ax = dfe.plot(column=variable, cmap='Reds', linewidth=0.8, ax=ax, edgecolor='0.3', legend = False, alpha=0.75)  # , legend_kwds={'label': "Capital", 'orientation': "horizontal"})
    ctx.add_basemap(ax, url=ctx.sources.ST_TERRAIN_BACKGROUND)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    dfe['index1'] = dfe.index
    #dfe = dfe.set_geometry('centroid_column')
    #dfe['centroid_column'].plot(ax=ax, markersize=dfe[variable]*50, color='k', zorder=1, alpha = 0.5)
    dfe.sort_values(by=[variable], inplace=True, ascending=True)
    for idx, row in dfe.iterrows():
        incidence = str("\n{:.0f}".format(row[variable])) if row[variable]!=0 else ""
        line = row['index1'] + incidence
        offset = 0
        offset = -75000 if row['index1'] == 'Capital and Coast' else offset
        offset =  25000 if row['index1'] == 'Wairarapa' else offset
        #print(line, row.x, row.y)
        ax.text(row.x, row.y + offset, s=line, horizontalalignment='center', fontweight='bold', fontsize=10, path_effects=[pe.withStroke(linewidth=3, foreground="white")])
    file_out = '../pictures/' + fn + '.png'
    #print(file_out)
    fig.savefig(file_out, dpi=dpi)
    matplotlib.rcParams['text.usetex'] = False
    return(dfe)

def read_web_data_nz_detailed(url):
    html = urlopen(url)
    soup = BeautifulSoup(html.read(), 'lxml')
    paras = soup.find_all('p')
    dag = makemyday(paras)
    tables = soup.find_all('table')  # [0] # Grab the first table
    table = tables[0]
    data = []
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        print(cols)
        if len(cols)>0:
            data.append(cols)
    df = pd.DataFrame(data, columns=['date', 'sex', 'agegroup', 'dhb', 'overseas', 'last city', 'flight', 'departure', 'arrival'])
    df['status'] = 'c'
    table = tables[1]
    data = []
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        print(cols)
        if len(cols)>0:
            data.append(cols)
    df = df.append(pd.DataFrame(data, columns=['date', 'sex', 'agegroup', 'dhb', 'overseas', 'last city', 'flight', 'departure', 'arrival']))
    df['status'].fillna('p', inplace=True)
    df['date'] = pd.to_datetime(df['date'], format="%d/%m/%Y")
    #df['id'] = df['id'].apply(pd.to_numeric, errors='coerce').fillna(0)
    df.set_index('date', inplace=True)
    fn = 'nz_corona_detailed_' + dag.strftime('%d-%m-%Y_%H%M')
    df['datum'] = dag # datetime.datetime.strptime(dag, '%d-%m-%Y_%H:%M')
    df.loc[df['dhb'].str.contains('Hawke'), 'dhb'] = "Hawke's Bay"
    df.loc[df['dhb'].str.contains('Southern'), 'dhb'] = "Southern"
    df.loc[df['agegroup'].str.contains('15'), 'agegroup']= "15 to 19"
    df.loc[df['agegroup'].str.contains('20'), 'agegroup']= "20 to 29"
    df.loc[df['agegroup'].str.contains('40'), 'agegroup']= "40 to 49"
    df.loc[df['agegroup'].str.contains('60'), 'agegroup']= "60 to 69"
    df.loc[df['agegroup'].str.contains('to 9'), 'agegroup']= "05 to 9"
    df.loc[df['agegroup'].str.contains('1 to 4'), 'agegroup']= "01 to 4"
    df.loc[df['agegroup'].str.contains('<1'), 'agegroup']= "0 to 1"
    df['dhb'] = df['dhb'].str.replace("&", "and")
    df['dhb'] = df['dhb'].str.replace("-", " ")
    #df['location'] = df['location'].str.replace("DHB", "")
    df.to_csv('../data/' + fn + '.csv')
    print(df.dhb.value_counts().sort_index())
    return(df, fn, dag)


def read_web_data_nz(url):
    html = urlopen(url)
    soup = BeautifulSoup(html.read(), 'lxml')
    paras = soup.find_all('p')
    dag = makemyday(paras)
    table = soup.find_all('table')  # [0] # Grab the first table
    table = table[1]
    data = []
    rows = table.find_all('tr')
    for row in rows:
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        print(cols)
        if len(cols)>0 and 'Total' not in cols:
            data.append(cols)
    #df = pd.DataFrame(data, columns=['id', 'location', 'age', 'gender', 'narrative'])
    df = pd.DataFrame(data, columns=['DHB', 'Total', 'New'])
    df['Total'] = df['Total'].apply(pd.to_numeric, errors='coerce').fillna(0)
    df.set_index('DHB', inplace=True)
    fn = 'nz_corona_' + dag.strftime('%d-%m-%Y_%H:%M')
    df['datum'] = dag # datetime.datetime.strptime(dag, '%d-%m-%Y_%H:%M')
    df.to_csv('../data/' + fn + '.csv')
    return(df, fn, dag)

#%%
dfnz, fo, day = read_web_data_nz(url = 'https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-current-cases')
#%%
dfnzd, fod, dayd = read_web_data_nz_detailed(url = 'https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus/covid-19-current-situation/covid-19-current-cases/covid-19-current-cases-details')
#%%

dailygp = dfnzd.loc[dfnzd.status=='c'].groupby('date')
dailygp = dfnzd.loc[dfnzd.status=='p'].groupby('date')
dailygp = dfnzd.groupby('date')
dailygp.dhb.count().cumsum().plot()

dhbgp = dfnzd.groupby('dhb')
dhbgp.dhb.count().plot.bar()

sexgp = dfnzd.loc[dfnzd.status=='c'].groupby('sex')
sexgp.dhb.count().plot.bar()

agegp = dfnzd.groupby('agegroup')
agegp.dhb.count().plot.bar()


#%%
plt.figure(figsize=(10,5))
chart = sns.lineplot(data=dailygp.dhb.count().cumsum(), palette='Set1')
plt.xticks(rotation=90)
plt.legend(loc='upper right')
plt.yscale("log")


#%%
plt.figure(figsize=(10,5))
chart = sns.countplot(x="dhb", hue='sex', data=dfnzd.loc[dfnzd.status=='c'].sort_values(['dhb']).reset_index(drop=True), palette='Set1')
chart.set_xticklabels(chart.get_xticklabels(), rotation=45, horizontalalignment='right')
plt.legend(loc='upper right')

#%%
nz = read_nz_shape('nz_dhb_4326.shp', 'nz_cookie_mk2.shp')
nzm = nz[['Shape_Leng', 'geometry', 'centroid_column', 'x', 'y']].join(dfnz['Total'], how = 'right')
nzmap = make_plot(nzm, 'Total', fo, 300, fo)





#%%
## old code

dfb = pd.read_excel('../data/nz_corona_detailed_26-03-2020_1755_base.xls')
#dfb['base_date'] = pd.to_datetime(dfb['base_date'], format="%d/%m/%Y")
dfb['datum'] = pd.to_datetime(dfb['datum'])
dfb.set_index('id', inplace=True)
dfb = dfb[['base_date', 'datum']]

dailysumnz = dfb.groupby('base_date').count()
dailysumnz['datum'].plot()
dailysumnz.diff(axis=0, periods=1).datum.plot()

# 283
#%%
dfb = pd.read_excel('../data/covid-19-confirmed-cases-28mar20.xlsx', skiprows =[0,1,2])
dfb.rename(columns={'Report Date': 'date'}, inplace=True)
dfb.DHB.value_counts().sort_index()
dfb = dfb.set_index(['Report Date'])

dfb = dfb[dfb["Report Date"].between('2020-02-26','2020-03-27')]


dailysumnz = dfb.groupby('Report Date')['Sex'].count()
dailysumnz.plot()

dfb.loc["2020/02/26"]
dfb.loc['2020-02-26':'2020-03-27']
dfb[dfb.index.between('2020-02-26', '2020-03-27')]

dfb[(dfb['Report Date'] > '2000-6-1') & (dfb['Report Date'] <= '2000-6-10')]

