#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 16:30:41 2020

@author: martien
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gpd
import matplotlib.patheffects as pe
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pyproj import Proj, transform
import contextily as ctx
import matplotlib
from urllib.request import urlopen
from bs4 import BeautifulSoup
# from urllib.request import urlretrieve
import re
import ssl
import datetime
from datetime import date
from datetime import datetime
import glob
import seaborn as sns

# from pathlib import Path
ssl._create_default_https_context = ssl._create_unverified_context
regex = re.compile('(.*\/(?P<name>\w+)\.(?P<ext>\w+))')


# from pysal.contrib.viz import mapping as maps
# https://github.com/darribas/contextily/blob/master/contextily_guide.ipynb
# https://stackoverflow.com/questions/56559520/change-background-map-for-contextily
# https://towardsdatascience.com/lets-make-a-map-using-geopandas-pandas-and-matplotlib-to-make-a-chloropleth-map-dddc31c1983d
# http://darribas.org/gds15/content/labs/lab_03.html
# https://gis.stackexchange.com/questions/78838/converting-projected-coordinates-to-lat-lon-using-python
# https://stackoverflow.com/questions/54088858/plotting-a-map-using-geopandas-and-matplotlib
# https://stackoverflow.com/questions/19073683/matplotlib-overlapping-annotations-text
# https://www.volksgezondheidenzorg.info/sites/default/files/map/detail_data/klik_corona06032020.csv
# https://stackoverflow.com/questions/51120802/extract-text-between-two-different-tags-beautiful-soup


try:
    os.chdir('/home/martien/Documents/QGIS/NLGemeenten/Gemeentegrenzen_2019/')
except:
    os.chdir('/home/martien/Documents/QGIS/NLGemeenten/Gemeentegrenzen_2019/')

print('Pandas:  %s.' % pd.__version__)
print('Numpy:   %s.' % np.__version__)
print('Geopandas:   %s.' % gpd.__version__)

months = {
    'januari': 1,
    'februari': 2,
    'maart': 3,
    'april': 4,
    'mei': 5,
    'juni': 6,
    'juli': 7,
    'augustus': 8,
    'september': 9,
    'oktober': 10,
    'november': 11,
    'december': 12
}


def assemble():
    dft = pd.DataFrame()
    for file in glob.glob("../data/*.csv"):
        if "klik_corona" and "00:00.csv" in file:
            print(file)
            dfr = pd.read_csv(file)
            dfr = dfr.loc[dfr.id>0]
            dfr["datum"] = dfr["datum"].str.replace("_", " ")
            dfr['Gemeente'] = dfr['Gemeente'].str.replace("\(O\)", "")
            dfr['Gemeente'] = dfr['Gemeente'].str.replace(".", "")
            dfr['Gemeente'] = dfr['Gemeente'].str.strip()
            try:
                dfr["datum"] = pd.to_datetime(dfr["datum"], format='%d-%m-%Y %H:%M')
            except:
                dfr["datum"] = pd.to_datetime(dfr["datum"], format='%Y-%m-%d %H:%M')
            dft = dft.append(dfr)
    dft.drop('onbekend', axis=1, inplace=True)
    dft['id'] = dft['id'].astype(int)
    dft['datum'].value_counts().sort_index()
    dfp = dft.pivot('id', 'datum', 'Aantal').fillna(0).stack()
    dfp.name='Aantal'
    dfg = dft[['Gemeente', 'Pop', 'id']].dropna().drop_duplicates(subset ="Gemeente").set_index('id')
    dft.set_index('id', inplace=True)
    return(dft, dfg, dfp.to_frame())


def expo(drempel):
    table = dfp.reset_index().pivot('id', 'datum', 'Aantal')
    lyst = table[table.columns[-1]]
    lyst.name = 'Aantal'
    lyst = lyst.where(lyst > drempel).dropna()
    dfl = pd.DataFrame(index = lyst.index)
    dfl = dfl.join(table, how='inner')
    dfl.join(dfg['Gemeente'], how='inner').reset_index().set_index('Gemeente').drop('id', axis=1).T.plot()
    return(dfl)


def get_link(url):
    l = 0
    html = urlopen(url)
    soup = BeautifulSoup(html.read(), 'lxml')
    for link in soup.find_all('a', href=True):
        dlink = link.get('href')
        substr = re.search('.csv', dlink)
        if substr:
            #if regex.search(dlink).group('name').startswith('klik_'):
            print(dlink)
                # regex.search(dlink).group('name')
                # print(regex.search(dlink).group('ext'))
            l, naam = dlink, regex.search(dlink).group('name')
    return(l, naam)


def read_nederland(fn):
    inProj, outProj = Proj(init='epsg:3857'), Proj(init='epsg:4326')
    nederland = gpd.read_file(fn, encoding = 'utf-8')
    nederland = nederland.to_crs({'init': 'epsg:3857'})  # nederland = nederland.to_crs({'init': 'epsg:3395'}) #nederland = nederland.to_crs({'init': 'epsg:3035'})
    nederland['Code'] = nederland['Code'].apply(pd.to_numeric, errors='coerce').fillna(0)
    nederland.set_index('Code', inplace=True)
    nederland.rename(columns={'Gemeentena': 'Gemeente'}, inplace=True)
    nederland['coords'] = nederland['geometry'].apply(lambda x: x.representative_point().coords[:])  # https://stackoverflow.com/questions/38899190/geopandas-label-polygons
    nederland['coords'] = [coords[0] for coords in nederland['coords']]
    nederland[['lon', 'lat']] = pd.DataFrame(nederland['coords'].tolist(), index=nederland.index)  # https://stackoverflow.com/questions/29550414/how-to-split-column-of-tuples-in-pandas-dataframe
    nederland['lon4326'], nederland['lat4326'] = transform(inProj, outProj, nederland['lon'].tolist(), nederland['lat'].tolist())
    nederland['centroid_column'] = nederland.geometry.centroid
    nederland.plot()
    return(nederland)


def read_data_old(fn):  # dfo = read_data_old('klik_corona07032020.csv')
    df = pd.read_csv('../data/' + fn + '.csv', sep=";", decimal = ",", thousands = ".")
    df['id'] = df.astype({'id': 'int32'})
    df.set_index('id', inplace=True)
    df['Aantal'] = df['Aantal'].fillna(0)
    df.drop('Indicator', axis=1, inplace=True)
    df.to_csv('../data/' + fn + '_c.csv')
    return(df)


def read_data(url):
    link, fo = get_link(url)
    web = url.split('/')
    fn = web[0] + '//' + web[2] + link
    df = pd.read_csv(fn, sep=";", decimal = ",", thousands = ".")
    df['id'] = df.astype({'id': 'int32'})
    df.set_index('id', inplace=True)
    df['Aantal'] = df['Aantal'].fillna(0)
    df.drop('Indicator', axis=1, inplace=True)
    df.to_csv('../data/' + fo + '_c.csv')
    return(df, fo)


def read_web_data(url):
    html = urlopen(url)
    soup = BeautifulSoup(html.read(), 'lxml')
    container = soup.find('div', id='csvData')
    data = []
    for line in container.get_text().split("\n"):
        if "postcode" not in line:
            line = line.split(';')
            # print(line)
            if "peildatum" in line[0]:
                peildate = line[0]
                peildate = peildate.replace("peildatum", "").strip().split(' ')
                dag = str(peildate[0]) + '-' + str(months[peildate[1]]) + '-2020' + '_' + str(peildate[2])
            else:
                today = date.today()
                dag = today.strftime('%d-%m-%Y_%H:%M')
                if len(line) >2:
                    print(line)
                    data.append(line)
            #  print(len(line))
    df = pd.DataFrame(data[1:], columns=data[0]).dropna()
    # df["onbekend"] = df["onbekend"].str.replace(",", ".")
    df.rename(columns = {'Gemnr': 'id', 'BevAant': 'Pop'}, inplace =True)
    df.drop('Aantal per 100.000 inwoners', axis=1, inplace=True)
    df[['id', 'Aantal', 'Pop']] = df[['id', 'Aantal', 'Pop']].apply(pd.to_numeric, errors='coerce').fillna(0)
    df.set_index('id', inplace=True)
    fn = 'klik_corona_' + dag
    df['datum'] = dag # datetime.datetime.strptime(dag, '%d-%m-%Y_%H:%M')
    df.to_csv('../data/' + fn + '.csv')
    return(df, fn)


def add_basemap(ax, zoom, url='http://tile.stamen.com/terrain/tileZ/tileX/tileY.png'):
    xmin, xmax, ymin, ymax = ax.axis()
    basemap, extent = ctx.bounds2img(xmin, ymin, xmax, ymax, zoom=zoom, url=url)
    ax.imshow(basemap, extent=extent, interpolation='bilinear')
    ax.axis((xmin, xmax, ymin, ymax))


def make_plot(dfe, variable, fn, dpi):
    matplotlib.rcParams['text.usetex'] = True
    dfe[variable].fillna(0, inplace=True)
    # dfe, variable, fn, dpi = nederlandm, 'Aantal', fo, 300
    dfe['norm_' + variable] = (dfe[variable] - dfe[variable].min()) / (dfe[variable].max() - dfe[variable].min())
    dfe.dropna(subset=[variable], inplace=True)
    dfe.to_crs({'init': 'epsg:4326'})
    fig, ax = plt.subplots(1, figsize=(30, 30))
    #plt.title(fn.split('_')[1], fontsize=14)
    fn = fn.replace("klik_corona", "Corona").strip()
    plt.title(fn.replace("_", " "), fontsize=14)
    divider = make_axes_locatable(ax)
    ax = dfe.plot(column=variable, cmap='Reds', linewidth=0.8, ax=ax, edgecolor='0.3', legend = True, alpha=0.75)  # , legend_kwds={'label': "Capital", 'orientation': "horizontal"})
    ctx.add_basemap(ax, url=ctx.sources.ST_TERRAIN_BACKGROUND)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    dfe = dfe.set_geometry('centroid_column')
    dfe['centroid_column'].plot(ax=ax, markersize=dfe[variable]*50, color='k', zorder=1, alpha = 0.5)
    dfe.sort_values(by=[variable], inplace=True, ascending=True)
    for idx, row in dfe.iterrows():
        incidence = str("\n{:.0f}".format(row[variable])) if row[variable]!=0 else ""
        line = row['Gemeente'] + incidence
        ax.text(row.coords[0], row.coords[1], s=line, horizontalalignment='center', fontweight='bold', fontsize=8, path_effects=[pe.withStroke(linewidth=3, foreground="white")])
    file_out = '../pictures/' + fn + '.png'
    print(file_out)
    fig.savefig(file_out, dpi=dpi)
    matplotlib.rcParams['text.usetex'] = False
    return(dfe)


#%%
nederland = read_nederland("Gemeentegrenzen_2019.shp")
df, fo = read_web_data('https://www.rivm.nl/coronavirus-kaart-van-nederland#!node-coronavirus-covid-19-meldingen')
nederlandm = nederland.merge(df['Aantal'], left_index=True, right_index=True, how='left')
nl = make_plot(nederlandm, 'Aantal', fo, 300)
nl.Aantal.describe()
nl.Aantal.sum()
#%% Assemble data for analysis
dft, dfg, dfp  = assemble()
#%%
# Plot cities, but filter out cities with low total values
expo(100)

# Nice plots on totals per day
dailysum = dft.groupby('datum').sum()
dailysum.Aantal.plot(title = "Count")
dailysum.diff(axis=0, periods=1).Aantal.plot(title = "Growth")


#%%
