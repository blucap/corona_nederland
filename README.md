## Mapping corona Nederland

This script produces a heat-map with corona stats for the Netherlands.

You need this code and the relevant NL shapefile, which you should obtain from this site: [Gemeentegrenzen](https://hub.arcgis.com/datasets/e1f0dd70abcb4fceabbc43412e43ad4b_0?geometry=-32.347,49.742,42.975,54.460)

The script returns a dataframe with stats as well as a png map.

You have to create tow separate folders 'data' and 'pictures' to save the data and the png files.

> Written with [StackEdit](https://stackedit.io/). 
